const messages = [
  "prueba 1",
  "prueba 2",
  "prueba 3",
  "prueba 4",
  "prueba 5",
  "prueba 6",
  "prueba 7",
  "prueba 8",
  "prueba 9",
  "prueba 10"
]

exports.handler = async (event, context, callback) => {

  let message = messages[ Math.floor(Math.random() * 10)];

  const response = {
    statusCode: 200,
    timeStamp: Math.floor(+new Date() / 1000),
    body: message
  }

  callback(null,response);
};

